package expo.modules.readium

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PointF
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.commitNow
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import expo.modules.kotlin.AppContext
import expo.modules.kotlin.viewevent.EventDispatcher
import expo.modules.kotlin.views.ExpoView
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.runInterruptible
import kotlinx.coroutines.withTimeout
import org.json.JSONArray
import org.readium.r2.navigator.Decoration
import org.readium.r2.navigator.ExperimentalDecorator
import org.readium.r2.navigator.epub.EpubNavigatorFragment
import org.readium.r2.navigator.html.HtmlDecorationTemplates
import org.readium.r2.shared.ExperimentalReadiumApi
import org.readium.r2.shared.extensions.toList
import org.readium.r2.shared.extensions.toMap
import org.readium.r2.shared.publication.Locator
import org.readium.r2.shared.publication.html.cssSelector

@SuppressLint("ViewConstructor", "ResourceType")
@OptIn(ExperimentalDecorator::class, ExperimentalReadiumApi::class)
class EpubView(context: Context, appContext: AppContext) : ExpoView(context, appContext),
    EpubNavigatorFragment.Listener {

    private val templates = HtmlDecorationTemplates.defaultTemplates()
    val onLocatorChange by EventDispatcher()
    val onMiddleTouch by EventDispatcher()

    var bookService: BookService? = null
    var bookId: Int? = null
    var locator: Locator? = null
    var isPlaying: Boolean = false
    var navigator: EpubNavigatorFragment? = null

    fun initializeNavigator() {
        if (this.navigator != null) {
            return
        }

        val bookId = this.bookId ?: return
        val locator = this.locator ?: return
        val publication = bookService?.getPublication(bookId) ?: return

        val fragmentTag = resources.getString(R.string.epub_fragment_tag)
        val activity: FragmentActivity? = appContext.currentActivity as FragmentActivity?

        val listener = this
        activity?.supportFragmentManager?.commitNow {
            setReorderingAllowed(true)
            add(EpubFragment(locator, publication, isPlaying, listener), fragmentTag)
        }

        val fragment =
            activity?.supportFragmentManager?.findFragmentByTag(fragmentTag) as? EpubFragment
        addView(fragment?.view)

        navigator = fragment?.navigator
        activity?.lifecycleScope?.launch {
            activity.repeatOnLifecycle(Lifecycle.State.STARTED) {
                navigator?.currentLocator?.collect {
                    onLocatorChanged(it)
                }
            }
        }

    }

    fun go() {
        val navigator = this.navigator ?: return initializeNavigator()
        val locator = this.locator ?: return

        navigator.go(locator, true)
        if (isPlaying) {
            highlightSelection()
        }
    }


    fun highlightSelection() {
        val locator = this.locator ?: return
        val id = locator.locations.fragments.first()

        val overlayHighlight = Decoration.Style.Highlight(0xffffff00.toInt(), isActive = true)
        val decoration = Decoration(id, locator, overlayHighlight)

        runBlocking {
            navigator?.applyDecorations(listOf(decoration), "overlay")
        }

    }

    fun clearHighlights() {
        runBlocking {
            navigator?.applyDecorations(listOf(), "overlay")
        }
    }

    override fun onTap(point: PointF): Boolean {
        if (point.x < width * 0.2) {
            navigator?.goBackward(animated = true)
            return true
        }
        if (point.x > width * 0.8) {
            navigator?.goForward(animated = true)
            return true
        }
        onMiddleTouch(mapOf())
        return false
    }

    private suspend fun onLocatorChanged(locator: Locator) {
        if (isPlaying) {
            return
        }
        Log.d("EpubView", "Navigated to ${locator.locations.position}")

        val result = navigator?.evaluateJavascript(
            """
            (function() {
              function isOnScreen(element) {
                const rect = element.getBoundingClientRect();
                const isVerticallyWithin = rect.bottom >= 0 && rect.top <= window.innerHeight;
                const isHorizontallyWithin = rect.right >= 0 && rect.left <= window.innerWidth;
                return isVerticallyWithin && isHorizontallyWithin;
              }
              //debugger;
              let currentElement = document.body;
              while (currentElement?.children.length) {
                let currentChild = currentElement.firstElementChild;
                while (currentChild && !isOnScreen(currentChild)) {
                  currentChild = currentChild.nextElementSibling;
                }
                if (currentChild) {
                  currentElement = currentChild;
                } else {
                  currentElement = currentElement.nextElementSibling;
                }
              }
        
              if (!currentElement) return [];

              const fragments = [];

              while (currentElement) {
                if (currentElement.id) {
                  fragments.push(currentElement.id);
                }
                currentElement = currentElement.parentElement;
              }

              return fragments;
            })();
        """.trimIndent()
        )
        Log.d("EpubView", "result: $result")
        if (result == null) {
            return onLocatorChange(locator.toJSON().toMap())
        }
        @Suppress("UNCHECKED_CAST")
        val fragments = JSONArray(result).toList() as? List<String>
            ?: return onLocatorChange(locator.toJSON().toMap())
        val fragmentsLocator =
            locator.copy(locations = locator.locations.copy(fragments = fragments))
        onLocatorChange(fragmentsLocator.toJSON().toMap())
    }
}