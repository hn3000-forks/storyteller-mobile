import ExpoModulesCore
import R2Shared
import R2Navigator
import ReadiumAdapterGCDWebServer

// This view will be used as a native component. Make sure to inherit from `ExpoView`
// to apply the proper styling (e.g. border radius and shadows).
class EPUBView: ExpoView {
    private let templates = HTMLDecorationTemplate.defaultTemplates()
    let onLocatorChange = EventDispatcher()
    let onMiddleTouch = EventDispatcher()
    
    public var bookId: Int?
    public var locator: Locator?
    public var isPlaying: Bool = false
    public var navigator: EPUBNavigatorViewController?
    
    func initializeNavigator() {
        if self.navigator != nil {
            return
        }
        
        guard let bookId = self.bookId, let locator = self.locator else {
            return
        }
        
        guard let publication = BookService.instance.getPublication(for: bookId) else {
            print("skipping navigator init, publication has not yet been opened")
            return
        }
        
        let resources = Bundle.main.resourceURL!
        
        guard let navigator = try? EPUBNavigatorViewController(
            publication: publication,
            initialLocation: locator,
            config: .init(
                preferences: EPUBPreferences(
                    fontFamily: FontFamily(rawValue: "Bookerly"),
                    lineHeight: 1.4,
                    paragraphSpacing: 0.5
                ),
                defaults: EPUBDefaults(
                    publisherStyles: false
                ),
                decorationTemplates: templates,
                fontFamilyDeclarations: [
                    CSSFontFamilyDeclaration(
                        fontFamily: FontFamily(rawValue: "Bookerly"),
                        fontFaces: [
                            CSSFontFace(
                                file: resources.appendingPathComponent("Bookerly.ttf"),
                                style: .normal, weight: .standard(.normal)
                            ),
                        ]
                    ).eraseToAnyHTMLFontFamilyDeclaration(),
                ]
            ),
            httpServer: GCDHTTPServer.shared
        ) else {
            print("Failed to create Navigator instance")
            return
        }
        
        navigator.delegate = self
        addSubview(navigator.view)
        self.navigator = navigator
    }
    
    func go() {
        guard let navigator = self.navigator else {
            initializeNavigator()
            return
        }
        
        guard let locator = self.locator else {
            return
        }
        
        _ = navigator.go(to: locator, animated: true) {
            if self.isPlaying {
                self.highlightSelection()
            }
        }
    }
    
    func highlightSelection() {
        guard let locator = self.locator, let id = locator.locations.fragments.first else {
            return
        }
        
        let overlayHighlight = Decoration.Style.highlight(tint: .yellow, isActive: true)
        let decoration = Decoration(
            id: id,
            locator: locator,
            style: overlayHighlight)
        
        navigator?.apply(decorations: [decoration], in: "overlay")
    }
    
    func clearHighlights() {
        navigator?.apply(decorations: [], in: "overlay")
    }
    
    override func layoutSubviews() {
        guard let navigatorView = self.navigator?.view else {
            print("layoutSubviews called before navigator was instantiated")
            return
        }
        
        navigatorView.frame = bounds
      }
}

extension EPUBView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}

extension EPUBView: EPUBNavigatorDelegate {
    func navigator(_ navigator: R2Navigator.Navigator, presentError error: R2Navigator.NavigatorError) {
        // pass
    }
    
    func navigator(_ navigator: VisualNavigator, didTapAt point: CGPoint) {
        guard let navigator = self.navigator else {
            return
        }
        
        if point.x < bounds.maxX * 0.2 {
            _ = navigator.goBackward(animated: true) {}
            return
        }
        if point.x > bounds.maxX * 0.8 {
            _ = navigator.goForward(animated: true) {}
            return
        }
        
        onMiddleTouch()
    }
    
    func navigator(_ navigator: Navigator, locationDidChange locator: Locator) {
        if isPlaying {
            return
        }

        guard let epubNav = navigator as? EPUBNavigatorViewController else {
            return
        }
        
        epubNav.evaluateJavaScript("""
            (function() {
              function isOnScreen(element) {
                const rect = element.getBoundingClientRect();
                const isVerticallyWithin = rect.bottom >= 0 && rect.top <= window.innerHeight;
                const isHorizontallyWithin = rect.right >= 0 && rect.left <= window.innerWidth;
                return isVerticallyWithin && isHorizontallyWithin;
              }
              //debugger;
              let currentElement = document.body;
              while (currentElement?.children.length) {
                let currentChild = currentElement.firstElementChild;
                while (currentChild && !isOnScreen(currentChild)) {
                  currentChild = currentChild.nextElementSibling;
                }
                if (currentChild) {
                  currentElement = currentChild;
                } else {
                  currentElement = currentElement.nextElementSibling;
                }
              }
        
              if (!currentElement) return [];

              const fragments = [];

              while (currentElement) {
                if (currentElement.id) {
                  fragments.push(currentElement.id);
                }
                currentElement = currentElement.parentElement;
              }

              return fragments;
            })();
        """) {
            switch $0 {
            case .failure(_):
                self.onLocatorChange(locator.json)
            case .success(let anyValue):
                guard let value = anyValue as? [String] else {
                    self.onLocatorChange(locator.json)
                    return
                }
                
                if value.isEmpty {
                    self.onLocatorChange(locator.json)
                    return
                }
                
                self.onLocatorChange(
                    locator.copy(
                        locations: {
                            $0.otherLocations["fragments"] = value
                        }
                    ).json
                )
            }
        }
    }
}
