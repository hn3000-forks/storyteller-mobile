import { ScrollView, StyleSheet, View } from "react-native"
import { BookLink } from "../../../components/BookLink"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { HeaderText } from "../../../components/HeaderText"
import { MiniPlayerWidget } from "../../../components/MiniPlayerWidget"
import { useAppSelector } from "../../../store/appState"
import { getBookshelfBookIds } from "../../../store/selectors/bookshelfSelectors"

export default function Home() {
  const { top } = useSafeAreaInsets()
  const bookIds = useAppSelector(getBookshelfBookIds)

  return (
    <View style={{ ...styles.container, paddingTop: top }}>
      <HeaderText style={styles.title}>Bookshelf </HeaderText>
      <ScrollView style={styles.bookLinks}>
        {bookIds.map((bookId) => (
          <BookLink key={bookId} bookId={bookId} />
        ))}
        {/* Spacer for the miniplayer */}
        <View style={{ height: 76, width: "100%" }} />
      </ScrollView>
      <MiniPlayerWidget />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    marginVertical: 32,
    paddingLeft: 24,
    fontSize: 32,
    alignSelf: "flex-start",
  },
  bookLinks: {
    width: "100%",
    paddingLeft: 24,
  },
  logLink: {
    position: "absolute",
    top: 48,
    right: 24,
  },
})
