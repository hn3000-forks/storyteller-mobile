{
  description = "Example JavaScript development environment for Zero to Nix";

  # Flake inputs
  inputs = {
    nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.2305.491812.tar.gz"; # also valid: "nixpkgs"
  };

  # Flake outputs
  outputs = { self, nixpkgs }:
    let
      # Systems supported
      allSystems = [
        "x86_64-linux" # 64-bit Intel/AMD Linux
        "aarch64-linux" # 64-bit ARM Linux
        "x86_64-darwin" # 64-bit Intel macOS
        "aarch64-darwin" # 64-bit ARM macOS
      ];

      # Helper to provide system-specific attributes
      forAllSystems = f: nixpkgs.lib.genAttrs allSystems (system: f {
        pkgs = import nixpkgs { inherit system; };
      });
    in
    {
      # Development environment output
      devShells = forAllSystems ({ pkgs }: {
        default = let 
          cocoapods = pkgs.rubyPackages_3_2.cocoapods;
          nodejs = pkgs.nodejs_20;
          yarn = pkgs.yarn.override { inherit nodejs; };
          watchman = pkgs.watchman;
        in
        pkgs.mkShell {
          # The Nix packages provided in the environment
          packages = [
            nodejs
            yarn
            watchman
            cocoapods
          ];
        };
      });
    };
}
